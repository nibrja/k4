import java.text.NumberFormat;
import java.util.Objects;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {


   /** Main method. Different tests. */
   public static void main (String[] param) {
      valueOf("numerator=a" + ", denominator=5");
   }
   // Kasutatud allika:
   // https://www.cyberforum.ru/java-j2se/thread2179357.html
   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) throw new RuntimeException("Denominator cannot be zero");

      if (b < 0){
         a *= -1;
         b *= -1;
      }


      long limit = Math.min(a, b);
      if (limit < 0){
         limit *= -1;
      }

      for (long i = limit; i >= 2; i--) {
         if (a % i == 0 && b % i == 0){
            a /= i;
            b /=i;
         }
      }

      this.numerator = a;
      this.denominator = b;

   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return "numerator=" + numerator +
              ", denominator=" + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {

//      Lfraction sec = (Lfraction) m;
//
//      return this.simplify().toString().equals(sec.simplify().toString());

      return this.compareTo((Lfraction) m) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */

   @Override
   public int hashCode() {
      return Objects.hash(numerator, denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long numer = this.numerator * m.getDenominator() + this.denominator * m.getNumerator();
      long denom = this.denominator * m.getDenominator();

      if (numer == 0) return new Lfraction(numer , 1);
      return new Lfraction(numer, denom);
   }

   private Lfraction simplify(){
      long numer = this.numerator;
      long denom = this.denominator;


      long limit = Math.min(numer, denom);
      if (limit < 0){
         limit *= -1;
      }

      for (long i = limit; i >= 2; i--) {
         if (numer % i == 0 && denom % i == 0){
            numer /= i;
            denom /=i;
         }
      }

      return new Lfraction(numer, denom);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long numer = this.numerator * m.getNumerator();
      long denom = this.denominator * m.getDenominator();

      if (numer == 0) return new Lfraction(0, 1);
      return new Lfraction(numer, denom);
   }

   public Lfraction pow(int num){

      if (num < 0){
         return this.pow(-num).inverse();
      } else if (num == 0){
         return new Lfraction(1,1);
      } else {
         return this.times(this.pow(num - 1));
      }
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {

      if (numerator == 0) throw new RuntimeException("Denominator cannot be zero");
      return new Lfraction(this.denominator, this.numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-this.numerator, this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
//      long numer = this.numerator * m.getDenominator() - this.denominator * m.getNumerator();
//      long denom = this.denominator * m.getDenominator();

      return this.plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.getNumerator() == 0) throw new RuntimeException("Denominator cannot be zero");

      return this.times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */

   // Kasutatud allikad:
   // https://www.geeksforgeeks.org/program-compare-two-fractions/

   @Override
   public int compareTo (Lfraction m) {
      long num = this.numerator * m.getDenominator() - this.getDenominator() * m.getNumerator();
      if (num > 0){
         return 1;
      } else if (num < 0){
         return -1;
      }
      return 0;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.numerator / this.denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long num = this.numerator % this.denominator;

      if (num == 0) return new Lfraction(0, 1);
      return new Lfraction(this.numerator % this.denominator, this.denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) this.numerator / this.denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long n = (long) Math.ceil(f * d);
      return new Lfraction(n, d);

   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] a = s.split(",");


      long num;
      long denom;

      try {
         num =  Integer.parseInt(a[0].split("=")[1]);
         denom = Integer.parseInt(a[1].split("=")[1]);
      } catch (NumberFormatException e){
         throw new RuntimeException("Numerator and Denominator should be numbers: " + s);
      }

      if (denom == 0) throw new RuntimeException("Denominator cannot be zero: " + s);

      return new Lfraction(num, denom);
   }
}